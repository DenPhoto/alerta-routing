def rules(alert, plugins):

    if alert.severity in ['critical']:
        return [plugins['telegram']]
    else:
        return [plugins['rocketchat']]
